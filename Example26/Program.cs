﻿// Use commas in a for statement to find the largest and
// smallest factor of a number.
using System;
class Example
{
    static void Main()
    {
        int i, j;
        int smallest,largest;
        int num = 100;
        smallest = largest = 1;
        for (i = 2, j = num / 2; (j > 2) & (i <= j);i++,j--)
        {   

            if(smallest==1 && num % i == 0)
            {
                smallest = i;
            }
            if(largest==1 && num % j == 0)
            {
                largest = j;
            }
        }

        Console.WriteLine("smallest factor is {0} and largest factor is {1}", smallest, largest);
    }
}